<?php

require __DIR__ . '/vendor/autoload.php';


use \LINE\LINEBot\SignatureValidator as SignatureValidator;

// load config
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

// initiate app
$configs =  [
	'settings' => ['displayErrorDetails' => true],
];
$app = new Slim\App($configs);

/* ROUTES */
$app->get('/', function ($request, $response) {
	return "!";
});

$app->post('/', function ($request, $response)
{
	// get request body and line signature header
	$body 	   = file_get_contents('php://input');
	$signature = $_SERVER['HTTP_X_LINE_SIGNATURE'];

	// log body and signature
	file_put_contents('php://stderr', 'Body: '.$body);

	// is LINE_SIGNATURE exists in request header?
	if (empty($signature)){
		return $response->withStatus(400, 'Signature not set');
	}

	// is this request comes from LINE?
	if($_ENV['PASS_SIGNATURE'] == false && ! SignatureValidator::validateSignature($body, $_ENV['CHANNEL_SECRET'], $signature)){
		return $response->withStatus(400, 'Invalid signature');
	}

	//init bot
	$httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($_ENV['CHANNEL_ACCESS_TOKEN']);
	$bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $_ENV['CHANNEL_SECRET']]);
    $data = json_decode($body, true);
  
	foreach ($data['events'] as $event)
	{
        $userMessage = $event['message'];
        $message = $event['message']['text'];
        $messageId = $event['message']['id'];
        $userId = $event['source']['userId'];
        // $userName = $event['source']['displayName'];
        $replyToken = $event['replyToken'];

        if($message) {
            // $id = substr($message, -1);
            $id = explode("-", $message);
            $nameMenu = substr($message, 29, strlen($message));
            $urlAppInfo = 'https://gbalb-demo.cybozu.com/k/v1/records.json?app=544';
            $tokenAppInfo = 'X-Cybozu-API-Token:o0wTwW7qQxwTYG2vzevzenUzD9Hg3Re38rrb6kLo';
            $dataAppInfo = get($urlAppInfo, $tokenAppInfo);
            $recordApps = $dataAppInfo['records'];
            $arrInfo = [];
            $nameMenuSelect = [];
            foreach( $recordApps as $recordApp) {
                // $lengthMessage = 
                if($recordApp['Record_number']['value'] == $id[3]) {
                    // array_push($arrInfo, $recordApp['Text']['value']);
                    $date = substr($message, -30);
                    $a = (strlen($message)-23-strlen($id[3])-strlen($recordApp['Text']['value']));
                    $datetime = substr($message, $a, -(strlen($id[3]) +1 ));
                    array_push($nameMenuSelect, $recordApp['Text']['value']);
                    // $menu = substr($message, -8, -2);
                    $text = "Booked " . $datetime;
                    showActionConfirm($replyToken,$message, $text);
                }
                if($recordApp['Text']['value'] == $nameMenu) {
                    $menu = $recordApp['Text']['value'];
                    $date = substr($message, 7, -(strlen($menu) + 12));
                    $time = substr($message, 20, -(strlen($menu) + 1));
                    $idMenu = $recordApp['Record_number']['value'];
                    $datetime = $date. ' ' . $time;
                    $DateTime = new DateTime($datetime);
                    $DateTime->modify('+'. $recordApp['Text_1']['value'] . ' minutes');
                    $subtime = $DateTime->format("H:i:s");
                    array_push($nameMenuSelect, $recordApp['Text']['value']);

                    $urlAppInfo = 'https://gbalb-demo.cybozu.com/k/v1/records.json?app=533';
                    $tokenAppInfo = 'X-Cybozu-API-Token:p9Cl6mFK67XN6uHTJIAbBJZYBUcwFRdpqATcywwz';
                    $arrInfo = [];
                    $dataAppInfo = get($urlAppInfo, $tokenAppInfo);
                    $recordApps = $dataAppInfo['records'];

                    foreach( $recordApps as $recordApp) {
                        if($recordApp['Text_0']['value'] == $userId) {
                            array_push($arrInfo, $recordApp['Text']['value']);
                        }
                    }

                    addDataKintone($date, $time, $userId, $subtime, $menu, $idMenu, $arrInfo[0]);
                }
            }
            if(count($nameMenuSelect) > 0) {

                $length = (strlen($message)-24-strlen($nameMenuSelect[0]));
                $lengthMessage1 = (strlen($nameMenuSelect[0]) + 24 + $length);
                $lengthMessage2 = (strlen($nameMenuSelect[0]) + 29);
            } else {
                $length = strlen($message);
                $lengthMessage1 = 0;
                $lengthMessage2 = 0;
            }
            if($message != 'booking' && strlen($message) != $lengthMessage1 && strlen($message) != $lengthMessage2 )
            {
                $url = 'https://gbalb-demo.cybozu.com/k/v1/record.json';
                $data = [
                    "app" => '536',
                    "record" => [
                            "Text" => [
                                "value" => $message
                        ],
                        "Text_0" => [
                            "value" => $messageId
                        ],
                        "Text_1" => [
                            "value" => $userId
                        ]
                    ]
                    ];
                $postdata = json_encode($data);
                $token = 'X-Cybozu-API-Token: wyV9JWmBURrFxVB0vlQS6pSLEIDFS4HIIAjoqJEk';
                post($url, $postdata, $token);	
            }
            // $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($nameMenu);
            // $result = $bot->replyMessage($event['replyToken'], $textMessageBuilder);

        }
        if($message == 'booking') {
            
            $urlKintone = 'https://gbalb-demo.cybozu.com/k/v1/records.json?app=544';
            $tokenKintone = 'X-Cybozu-API-Token:o0wTwW7qQxwTYG2vzevzenUzD9Hg3Re38rrb6kLo';
            $records = get($urlKintone, $tokenKintone);
            $dataKintones = $records['records'];
            // $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($event);
            // $result = $bot->replyMessage($event['replyToken'], $textMessageBuilder);
            $arr = [];
            foreach($dataKintones as $dataKintone) {
                // $a = $dataKintone['Text']['value'];
                $title = $dataKintone['Text']['value'];
                $text = $dataKintone['Text_1']['value'] . ' minutes' . '-' .  $dataKintone['Text_0']['value'];
                $type = 'uri';
                $lable = $dataKintone['Text_2']['value'];
                $uri = $dataKintone['Text_3']['value'];
                
                $obj = [
                    "title"=> $title,
                    "text"=> $text,
                    "actions"=> [
                        [
                            "type" => "uri",
                            "label" => $lable,
                            "uri" => $uri
                        ]
                    ]
                ];
                array_push($arr, $obj);
                // array_push($arr, $obj = [
                //     "title"=> "aaaaa",
                //     "text"=> "vvvvv",
                //     "actions"=> [
                //         [
                //             "type" => "uri",
                //             "label" => "ccc",
                //             "uri" => "line://app/1653606453-D5o5NPxw"
                //         ]
                //     ]
                // ]);
            }

            $apiReply = 'https://api.line.me/v2/bot/message/reply';
            $dataReply = [
                "replyToken" => $event['replyToken'],
                "messages" => [
                    [
                    
                    "type"=> "template",
                    "altText"=> "this is a carousel template",
                    "template"=> [
                        "type"=> "carousel",
                        "columns" => $arr,
                        "imageAspectRatio"=> "rectangle",
                        "imageSize"=> "cover"
                    ]
                ]
            ]
        ];
        $jsonReply = json_encode($dataReply);
        $token = 'Authorization: Bearer Tro02pGYqiy8ADWr4Yo6Cb82KZnKjKWKXAEvaIDUghgkYyK5D09IB4T/ZUv4BDxQEPbpT5OMb1/5LHKT919G87Q1A54KyViU1NXS+kf+RhWLLaoeNnI1+6hGMwIB5D7+WRHnbVJgxthYvgf5Tvsi7gdB04t89/1O/w1cDnyilFU=';
        post($apiReply, $jsonReply, $token);
        }
        //     $datebooking = 'Date booked is '. $date . ' Time booked is ' . $time;
        //     $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($datebooking);
        //     $result = $bot->replyMessage($event['replyToken'], $textMessageBuilder);
        //     return $result->getHTTPStatus() . ' ' . $result->getRawBody();
        }
    
});

function post($url, $data, $token) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $token));
    $result = curl_exec($ch);
    curl_close($ch);
}

function showActionConfirm($replyToken,$message, $text) {
    $apiReply = 'https://api.line.me/v2/bot/message/reply';
    $dataReply = [
        "replyToken" => $replyToken,
        "messages" => [
                [
                    "type" => "text",
                    "text" => $message
                ],
                [
                "type" => "template",
                "altText" => "Booking",
                "template" => [
                    "type" => "confirm",
                    "text" => "Do you want to schedule?",
                    "actions" => [
                        [
                            "type" => "message",
                            "label" => "Yes",
                            "text" => $text
                        ],
                        [
                            "type" => "message",
                            "label" => "No",
                            "text" => "no"
                        ]
                    ]
                ]
                
            ]
        ]
    ];
    $jsonReply = json_encode($dataReply);
    $tokenReply = 'Authorization: Bearer Tro02pGYqiy8ADWr4Yo6Cb82KZnKjKWKXAEvaIDUghgkYyK5D09IB4T/ZUv4BDxQEPbpT5OMb1/5LHKT919G87Q1A54KyViU1NXS+kf+RhWLLaoeNnI1+6hGMwIB5D7+WRHnbVJgxthYvgf5Tvsi7gdB04t89/1O/w1cDnyilFU=';
    post($apiReply, $jsonReply, $tokenReply);
}

function addDataKintone($date, $time, $userId, $subtime, $menu, $idMenu, $userName) {
    $url = 'https://gbalb-demo.cybozu.com/k/v1/record.json';
    $data = [
        "app" => '541',
        "record" => [
            "Text" => [
                "value" => $date
            ],
            "Text_0" => [
                "value" => $time
            ],
            "Text_1" => [
                "value" => $userId
            ],
            "Text_2" => [
                "value" => $subtime
            ],
            "Text_3" => [
                "value" => $menu
            ],
            "Text_4" => [
                "value" => $idMenu
            ],
            "Text_5" => [
                "value" => $userName
            ]
        ]
        ];
    $postdata = json_encode($data);
    $token = 'X-Cybozu-API-Token: tdLtii6LPOCPvRTWfWwi7VooirVdZAM6BRTErgl8';
    post($url, $postdata, $token);
}

function get($url, $token) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array( $token ));
    $result = curl_exec($ch);
    $kintone = json_decode($result, true);
    curl_close($ch);
    return $kintone;
}

$app->run();